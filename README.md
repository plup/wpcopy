# WP COPY

## Requirements

This script relies on:

* `SSH` authentication from target server to origin
* `wpcli` on both, origin and target, servers:
```
$ curl https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -o ~/.local/bin/wp
$ chmod +x ~/local/bin/wp
```

## Install

Drop script on target server:
```
$ curl -O https://gitlab.com/plup/wpcopy/raw/master/wpcopy.sh
```

## Configuration

### Parameters

Configure parameters:
```
$ vi wpcopy.conf

## Mandatory
origin_host=example.org
origin_user=sshuser
origin_wordpress_directory=/var/www/wordpress
local_wordpress_directory=/var/www/wordpress

## Optional
#new_wordpress_url=
#old_wordpress_url=
# defaults to 'wp'
#origin_wpcli_bin=
#local_wpcli_bin=
```

### Custom files

Custom files override the wordpress content after the copy is made. The whole content of the directory specified with the `-o|--override` option is recursively copied to the local wordpress directory. This allows to specify different configurations between the two environments.

For example, it can be used to define a `wp-config.php` with the configuration relevant to the local wordpress instance. Without it, wordpress will keep the parameters from the original instance and database import will fail if those parameters are not identical in both instance.

Launch script:
```
$ bash wpcopy.sh -c wpcopy.conf -o localoverride --files --database
```

Have a beer !
```
   _.._..,_,_
  (          )
   ]~,"-.-~~[
 .=])' (;  ([
 | ]:: '    [
 '=]): .)  ([
   |:: '    |
    ~~----~~
```
