#!/bin/bash
set -e

load_config() {
    if [[ ! -e "$config" ]]; then
        echo "Config file '$config' not found"
        exit 1
    fi
    . "$config"
}

copy_files() {
    echo -n 'Copying files... '
    nice -n 10 rsync --compress --delete-after --archive --partial \
        "${origin_user:?}@${origin_host:?}:${origin_wordpress_directory:?}/" \
        "${local_wordpress_directory:?}"
    echo OK
}

override_config() {
    if [[ ! -d "$override" ]]; then
        echo "No local override"
    else
        echo -n 'Overriding with local configuration... '
        cp -rT ${override} ${local_wordpress_directory:?}
        echo "OK"
    fi
}

copy_db() {
    echo 'Copying database... '

    dump_file=dump_wp_$(date +%F)

    # Dump on oririn
    ssh ${origin_user:?}@${origin_host:?} "${origin_wpcli_bin:-wp} --path=${origin_wordpress_directory:?} db export \$HOME/$dump_file"

    # Copy dump to destination
    nice -n 10 rsync --compress --delete-after --archive --partial \
        ${origin_user:?}@${origin_host:?}:\$HOME/$dump_file \
        $HOME/$dump_file

    # Remove dump on origin
    ssh ${origin_user:?}@${origin_host:?} "rm \$HOME/$dump_file"

    # Import dump locally
    ${local_wpcli_bin:-wp} --path=${local_wordpress_directory:?} db import $HOME/$dump_file

    # Remove local dump
    rm $HOME/$dump_file

    echo "OK"
}

update_tables() {
    if [[ -z "$new_wordpress_url" ]]; then
        echo "No site url update required"
    else
        echo "Updating tables... "

        # Update parameters for migration to new URL
        ${local_wpcli_bin:-wp} --path=${local_wordpress_directory:?} option update home ${new_wordpress_url:?}
        ${local_wpcli_bin:-wp} --path=${local_wordpress_directory:?} option update siteurl ${new_wordpress_url:?}
        ${local_wpcli_bin:-wp} --path=${local_wordpress_directory:?} --skip-columns=guid search-replace ${old_wordpress_url:?} ${new_wordpress_url:?}

       echo "OK"
   fi
}

# parsing args
usage() {
	echo "Usage: $0 [-c|--config config_file] [-d|--database] [-f|--files]"
}

if [[ "$#" == 0 ]]; then
    usage
fi

while [[ "$#" > 0 ]]; do case $1 in
  -c|--config) config="$2"; shift;;
  -o|--override) override="$2"; shift;;
  -d|--database) database=1;;
  -f|--files) files=1;;
  *) echo "Unknown parameter: $1"; usage; exit 1;;
esac; shift; done

# orchestration
if [[ "$files" == 1 ]]; then
    load_config
    copy_files
    override_config
fi

if [[ "$database" == 1 ]]; then
    load_config
    copy_db
    update_tables
fi
